package iptracker

import (
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"log"
)

type Record struct {
	Guid               string `json:"guid"`
	Host               string `json:"host"`
	PrivateIp          string `json:"privateIp"`
	PublicIp           string `json:"publicIp"`
	VpnIp              string `json:"vpnIp,omitempty"`
	Timestamp          string `json:"timeStamp"`
	IsMostRecentRecord bool   `json:"isMostRecentRecord"`
}

func GetMostRecentDbRecordForHost(host string) (Record, error) {
	log.Printf("Debug: ++getMostRecentDbRecordForHost()")
	defer log.Printf("Debug: --getMostRecentDbRecordForHost()")

	var ret Record

	ses, err := session.NewSession()
	svc := dynamodb.New(ses)

	filt := expression.Name("isMostRecentRecord").Equal(expression.Value(true))

	proj := expression.NamesList(expression.Name("guid"), expression.Name("host"), expression.Name("privateIp"), expression.Name("publicIp"), expression.Name("vpnIp"), expression.Name("timeStamp"), expression.Name("isMostRecentRecord"))

	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()

	if err != nil {
		log.Printf("Error: failed to build expression: " + err.Error())
		return ret, errors.New("failed to build expression")
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String("ip_tracker"),
	}

	// Make the DynamoDB Query API call
	result, err := svc.Scan(params)
	if err != nil {
		log.Printf("Error: failed to query dynamodb: " + err.Error())
		return ret, errors.New("failed to query dynamodb")
	}

	foundMatch := false

	for _, i := range result.Items {
		item := Record{}

		err = dynamodbattribute.UnmarshalMap(i, &item)

		if err != nil {
			log.Printf("Error: failed to unmarshal result: " + err.Error())
			return ret, errors.New("failed to unmarshal result")
		}

		if item.Host == host {
			foundMatch = true
			ret = item
		}
	}

	if !foundMatch {
		return ret, errors.New("failed to find any matching results for host " + host)
	}

	return ret, nil
}

func DeleteRecordWithGuid(guid string) error {
	log.Printf("Debug: ++deleteRecordWithGuid()")
	defer log.Printf("Debug: --deleteRecordWithGuid()")

	ses, err := session.NewSession()
	svc := dynamodb.New(ses)

	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"guid": {
				S: aws.String(guid),
			},
		},
		TableName: aws.String("ip_tracker"),
	}

	_, err = svc.DeleteItem(input)
	if err != nil {
		log.Printf("Error: failed to delete item with guid " + guid + ": " + err.Error())
		return errors.New("failed to delete item with guid " + guid)
	}

	return nil
}